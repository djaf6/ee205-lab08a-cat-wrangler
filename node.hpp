///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file node.hpp
/// @version 1.0
///
/// A generic Node class.  May be used as a base class for a Doubly Linked List
///
/// @author Destynee Fagaragan <djaf6@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   04/12/21
///////////////////////////////////////////////////////////////////////////////

#pragma once

class Node {
friend class DoubleLinkedList;

protected:
   Node* prev = nullptr;
	Node* next = nullptr;

public:
   void dump() const;
	virtual bool operator>(const Node& r);

}; // class Node
