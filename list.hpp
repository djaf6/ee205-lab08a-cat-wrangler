///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.hpp
/// @version 1.0
///
/// @author Destynee Fagaragan <djaf6@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   04/12/21
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "node.hpp"

using namespace std;

class DoubleLinkedList {
protected:
   Node* head = nullptr;
   Node* tail = nullptr;
   unsigned int count = 0;

public:
   const bool empty() const;                             
   inline unsigned int size() const {              
      return count;
   }
   
   Node* get_first() const;                              
   Node* get_next( const Node* currentNode ) const;      
   Node* get_prev( const Node* currentNode ) const;      
   Node* get_last() const;                               
   
   void push_front( Node* newNode );                     
   Node* pop_front();                                    
   void push_back( Node* newNode );                      
   Node* pop_back();                                     
   
   bool validate();
   void dump() const;

   void swap(Node* node1, Node* node2);

   const bool isSorted() const;
   void insertionSort();

   bool isIn(Node* someNode) const;
   void insert_after(Node* currentNode, Node* newNode);
   void insert_before(Node* currentNode, Node* newNode);

};


